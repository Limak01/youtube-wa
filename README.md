# YouTube without account

Browser extension that allows you to follow your favorite YouTube creators
without account.

![Screen from extension](/screenshots/s1.png?raw=true "Feed view screenshot")

# Installation 

Download [dist.crx](https://gitlab.com/Limak01/youtube-wa/-/blob/master/dist.crx) then open extension settings in your borwser and drag and drop downloaded file to browser.

Download source code for [server](https://gitlab.com/Limak01/youtube-wa-server) or git clone:
     
     # Installation
     git clone https://gitlab.com/Limak01/youtube-wa-server.git
     cd youtube-wa-server
     sudo make install

     # Enable and start youtube-wa service
     sudo systemctl enable youtube-wa.service
     sudo systemctl start youtube-wa.service

     # Removal
     sudo make uninstall

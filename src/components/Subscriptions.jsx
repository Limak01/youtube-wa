import { createSignal, For, onMount } from "solid-js"
import styles from "../css/Subscriptions.module.css"
import ChannelBar from "./ChannelBar"

function Subscriptions({subscriptions, setSubscriptions}) {
    //const [subscriptions, setSubscriptions] = createSignal(JSON.parse(localStorage.getItem("subs")) || [])

    return (
        <div class={styles.container}>
            <For each={subscriptions()}>
                {(item) => <ChannelBar channelData={() => item} subscriptions={subscriptions} setSubscriptions={setSubscriptions}/>}
            </For>
        </div>
    )
}

export default Subscriptions
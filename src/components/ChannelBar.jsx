import { createSignal, createEffect } from "solid-js"
import styles from "../css/ChannelBar.module.css"
import proxy from "../proxy"

function ChannelBar({channelData, setSubscriptions, subscriptions}) {
    const [isSubscribed, setIsSubscribed] = createSignal(false)
    const [isLoading, setIsLoading] = createSignal(true)
    const subscribeUrl = `${proxy}/subscribe`
    const unsubUrl = `${proxy}/unsubscribe`

    createEffect(() => {
        const isSub = subscriptions().find((item, index) => item.id === channelData()?.id)
        
        if(isSub)
            setIsSubscribed(true)
    })


    const handleSubscribe = () => {
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(channelData()),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        fetch(subscribeUrl, fetchOptions)
        setSubscriptions([...subscriptions(), channelData()])
        setIsSubscribed(true)
    }

    const handleUnsubscribe = () => {
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(channelData()),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const subs = subscriptions()
        const filteredSubs = subs.filter((item, index) => item.id !== channelData()?.id)

        fetch(unsubUrl, fetchOptions)
        setSubscriptions(filteredSubs)
        setIsSubscribed(false)
    }

    const formatNumber = (number) => {
        if(number < 1000) return number
        if(number < 100_000) return (number / 1000).toFixed(1) + "K"
        if(number < 1_000_000) return Math.round(number / 1000) + "K"
        if(number < 10_000_000) return (number / 1_000_000).toFixed(2) + "M";
        if(number < 100_000_000) return (number / 1_000_000).toFixed(1) + "M";
        if(number < 1_000_000_000) return Math.round((number / 1_000_000)) + "M";
    }

    return (
        <div class={styles.ChannelBar}>
            <div class={styles.channelInfo} >
                <a href={`https://youtube.com/channel/${channelData()?.id}`} target="_blank">
                    <div class={styles.image} >
                        {isLoading() && <div class="loading" style="width: 100%; height: 100%;"/>}
                        <img 
                            onLoad={() => setIsLoading(false)}
                            src={channelData()?.thumbnail} alt={`${channelData()?.name} channel thumbnail`}
                        />
                    </div>
                </a>
                <div class={styles.desc}>
                    {
                        isLoading() ? 
                        <>
                            <div class="loading" style="width: 150px; height: 15px; margin-bottom: 5px;"/>
                            <div class="loading" style="width: 100px; height: 12px;"/>
                        </>
                        :
                        <>
                            <a href={`https://youtube.com/channel/${channelData()?.id}`} target="_blank">
                                <span class={styles.name}>
                                    {channelData()?.name}
                                </span>
                            </a> 
                            <span class={styles.subscribers}>
                                {formatNumber(parseInt(channelData()?.subs))} subscribers
                            </span>
                        </>
                    }
                </div>
            </div>
            {
                isSubscribed() ? 
                <button 
                    class={`${styles.subscribeButton} ${styles.subscribed}`}
                    onClick={handleUnsubscribe}>
                    SUBSCRIBED
                </button>
                :
                <button 
                    class={styles.subscribeButton}
                    onClick={handleSubscribe}>
                    SUBSCRIBE
                </button>
            }
        </div>
    )
}

export default ChannelBar

import { createEffect, createSignal, For } from "solid-js"
import styles from "../css/Feed.module.css"
import proxy from "../proxy"

function Feed({videos, setVideos}) {
    //const [videos, setVideos] = createSignal([])

    createEffect(() => fetchVideoData().then(videoData => setVideos(videoData)))

    return (
        <div class={styles.Feed}>
           <For each={videos()}>
                {(video) => <a href={video?.link} target="_blank"><Item video={video}/></a>}
            </For>
        </div>
    )
}

function Item({video}) {
    const [isLoading, setIsLoading] = createSignal(true)
    return (
        <div class={styles.item}>
            <div class={styles.image}>
                {isLoading() && <div class="loading" style="width: 100%; height: 100%;"/>}
                <img 
                    src={video?.thumbnail.replace("hqdefault", "mqdefault")} 
                    width="100%"
                    onLoad={() => setIsLoading(false)}/>
            </div>
            <div class={styles.description}>
                {
                    isLoading() ? 
                    <>
                        <div class="loading" style="width: 150px; height: 15px; margin-bottom: 5px;"/>
                        <div class="loading" style="width: 100px; height: 12px;"/>
                    </>
                    :
                    <>
                       <span 
                            class={styles.title}
                            title={video?.title}>
                        {video?.title.length > 50 ? video?.title.substr(0, 50) + "..." : video?.title}
                        </span>
                        <div class={styles.info}>
                            <span class={styles.infoText}>{video?.author}</span>
                            <span class={styles.separator}>&#8226;</span>
                            {/* <span class={styles.infoText}>127K views</span>
                            <span class={styles.separator}>&#8226;</span> */}
                            <span class={styles.infoText}>{formatDate(video?.pubDate)}</span>
                        </div> 
                    </>
                }
            </div>
        </div>
    )
}

export async function fetchVideoData() {
    const fetchUrl = `${proxy}/feed`
    const videosData = await fetch(fetchUrl).then(res => res.json())
    
    return videosData
}

const formatDate = (date) => {

    const currentDate = new Date()
    const prevDate = new Date(date)

    const seconds = Math.floor((currentDate - prevDate) / 1000)
    const minutes = Math.floor(seconds / 60)
    const hours = Math.floor(minutes / 60)
    const days = Math.floor(hours / 24)
    const weeks = Math.floor(days / 7)
    const months = Math.floor(days / 31)
    const years = Math.floor(months / 12)

    if(years >= 1) 
        return `${years} year${years > 1 ? 's' : ""} ago`
    
    if(months >= 1) 
        return `${months} month${months > 1 ? 's' : ""} ago`

    if(weeks >= 1) 
        return `${weeks} week${weeks > 1 ? 's' : ""} ago`
    
    if(days >= 1) 
        return `${days} day${days > 1 ? 's' : ""} ago`
    
    if(hours >= 1) 
        return `${hours} hour${hours > 1 ? 's' : ""} ago`
    
    if(minutes >= 1) 
        return `${minutes} minute${minutes > 1 ? 's' : ""} ago`
    
    if(seconds >= 10) 
        return `${seconds} seconds ago`
    

    return "now"
}

export default Feed
import styles from "../css/TopBar.module.css"
import RefreshIcon from "../assets/svg/refresh.svg"
import SubscriptionsIcon from "../assets/svg/subscriptions.svg"
import BackIcon from "../assets/svg/back.svg"
import { fetchVideoData } from "./Feed"

function TopBar(props) {
    const handleRefresh = () => fetchVideoData().then(videoData => props.setVideos(videoData))

    return (
        <div class={styles.TopBar}>
            <h1 class={styles.title}>{props.content()}</h1>
            <div class={styles.buttons}>
                {
                    props.content() === 'Subscriptions' ?
                    <div 
                        class={styles.btn} 
                        title="Back"
                        onClick={() => props.setContent('Feed')}>
                        <BackIcon class={styles.icon} />
                    </div>
                    :
                    <>
                        <div 
                            class={styles.btn} 
                            title="Refresh"
                            onClick={handleRefresh}>
                            <RefreshIcon class={styles.icon} />
                        </div>
                        <div 
                            class={styles.btn} 
                            title="Subscriptions"
                            onClick={() => props.setContent('Subscriptions')}>
                            <SubscriptionsIcon />
                        </div>
                    </>
                }
            </div>
        </div>
    )
}

export default TopBar
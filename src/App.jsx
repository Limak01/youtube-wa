import { createSignal, createEffect, on } from "solid-js";
import ChannelBar from "./components/ChannelBar";
import Feed from "./components/Feed";
import Subscriptions from "./components/Subscriptions";
import TopBar from "./components/TopBar"
import proxy from "./proxy"


const api = `https://invidious.namazso.eu/api/v1`

function App() {
  const [content, setContent] = createSignal('Feed')
  const [channelData, setChannelData] = createSignal({})
  const [subscriptions, setSubscriptions] = createSignal([])
  const [videos, setVideos] = createSignal([])
  const [url, setUrl] = createSignal("")

  const fetchSubscriptions = async () => {
    const subscriptions = await fetch(`${proxy}/subscriptions`).then(res => res.json())

    setSubscriptions(subscriptions)
  }

  fetchSubscriptions()
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    const activeTab = tabs[0];
    setUrl(activeTab.url)
  });

 createEffect(() => {
  if(isYouTube(url()))
    getChannelData(url()).then(res => setChannelData(res))
 })


  //getChannelData(url()).then(res => setChannelData(res))

  return (
    <div className="container">
      <TopBar setContent={setContent} content={content} setVideos={setVideos}/>
      {
        content() === 'Feed' ? 
        <>
          <Feed videos={videos} setVideos={setVideos}/>
          { (isYouTube(url()) && channelData() !== false) && <ChannelBar channelData={channelData} setSubscriptions={setSubscriptions} subscriptions={subscriptions}/> }
        </>
        : 
        <Subscriptions subscriptions={subscriptions} setSubscriptions={setSubscriptions}/>
      }
    </div>
  );
}

async function getChannelData(www) {
  if(www === "") return false

  const channelParams = "?fields=author,authorId,authorThumbnails,subCount"

  const url = new URL(www)
  const path = url.pathname.split("/")[1]
  const channel = url.pathname.split("/")[2]
  let data

  if(path === "" || path === "results") return false
  
  const fetchData = url => {
   const result = fetch(url)
                  .then(res => res.json())
                  .catch(err => console.log("Error getting channel data!"))

    return result
  }

  if(url.searchParams.get("v")) {
    const videoId = url.searchParams.get("v")
    const videoData = await fetchData(`${api}/videos/${videoId}?fields=authorId`)
    const channelID = videoData.authorId

    data = await fetchData(`${api}/channels/${channelID}${channelParams}`)
    console.log("data: ", data)
  }

  if(path === 'channel') 
    data = await fetchData(`${api}/channels/${channel}${channelParams}`)

  if(path === "c" || path == "user") {
    const results = await fetchData(`${api}/search?q=${channel}&type=channel&fields=author,authorId,authorThumbnails,subCount`)
    results[0].authorThumbnails[0].url = "https:" + results[0].authorThumbnails[0].url
    data = results[0]
  }
  
  return {
    id: data.authorId,
    thumbnail: data.authorThumbnails[0].url,
    name: data.author,
    subs: data.subCount,
  }
}

function isYouTube(url) {
  if(url === "") return false

  let hostname = new URL(url).hostname

  hostname = hostname.replace("www.", "")
  
  if(hostname === "youtube.com")
    return true


  return false
}

export default App;
